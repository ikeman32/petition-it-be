const db = require('../database/dbConfig.js');

const tbl = 'petition';//define table name

module.exports = {
  add,
  find,
  findBy,
  findById,
};

function find() {
  return db(tbl).select('name', 'city', 'state', 'signature' );
}

function findBy(filter) {
  return db(tbl).where(filter);
}

async function add(user) {
  console.log(user);
  const [id] = await db(tbl).insert(user);
  
  return findById(id);
}

function findById(id) {
  return db(tbl)
    .where({ id })
    .first();
}
