const router = require('express').Router();

const Signed = require("./petition-model");

//const restricted = require('../auth/restricted-middleware.js');//for basic authorization

router.get("/", (req, res) => {
  Signed.find()
    .then(sig => {
      res.status(200).json(sig);
    })
    .catch(err => {
      res.status(404).json(err);
    });
});

router.post('/sign', (req, res) => {
  let sign = req.body;
  Signed.add(sign)
    .then(sig =>{
      res.status(201).json(sig);
    })
    .catch(err =>{
      res.status(500).json(err);
    });
});

module.exports = router;
