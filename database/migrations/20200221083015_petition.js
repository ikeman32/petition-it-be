
exports.up = function(knex) {
    return knex.schema.createTable('petition', tbl =>{
          tbl.increments('id').primary();

          tbl.string('name', 128).notNullable();
          tbl.string('city', 128).notNullable();
          tbl.string('state', 128).notNullable();
          tbl.string('signature').notNullable();
      });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('petition');
};
